﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32.TaskScheduler;

namespace VMSService
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //import dll
        [DllImport("PowrProf.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetSuspendState(bool hiberate, bool forceCritical, bool disableWakeEvent);

        //constant and flag
        private bool _running = false;
        private const byte QUIT_FLAG = 0xff;
        public string masterIp { get; set; }
        private const string dir = @"C:\VMS\";

        //clase initialization
        UDPService udp = new UDPService();
        Thread udpProcessingThread;
        private System.Timers.Timer timer1 = null;

        public MainWindow()
        {
            InitializeComponent();

            //start thread
            udpProcessingThread = new Thread(processUDPData);
            udpProcessingThread.Start();
            Library.WriteErrorLog("UDP thread started");

            //start timer
            timer1 = new System.Timers.Timer();
            this.timer1.Interval = 30000;
            this.timer1.Elapsed += new ElapsedEventHandler(this.timer1_Tick);
            timer1.Enabled = true;
            Library.WriteErrorLog("Timer service started");

            txtStatus.Content = "Started";
            txtStatus.Background = Brushes.GreenYellow;
            txtStatus.FontStyle = FontStyles.Normal;
        }

        private void timer1_Tick(object sender, ElapsedEventArgs e)
        {
            string ping = string.Empty;
            Process[] pname = Process.GetProcessesByName("VMS_Client");
            if (pname.Length == 0)
            {
                ping = "Process not found";
                byte[] bytes = ASCIIEncoding.ASCII.GetBytes(ping);
                udp.send(masterIp, bytes, (uint)bytes.Length);
            }

        }

        private void Status(string status, string ip)
        {
            DateTime now = DateTime.Now;
            txtLog.Text = status + " command received from " + ip + " on " + now;
        }

        public void processUDPData()
        {
            byte[] rawdata = new byte[1024];
            uint size;
            EndPoint ipinfo = new IPEndPoint(0, 0);
            string data, ip;

            _running = true;

            Library.WriteErrorLog("UDP thread started successfully");

            while (_running)
            {
                Array.Clear(rawdata, 0, rawdata.Length);
                size = (uint)rawdata.Length;
                udp.recv(ref rawdata, ref size, ref ipinfo);

                if (rawdata[0] == QUIT_FLAG) break; //stop processing & quit upon receiving quit order

                ip = (ipinfo as IPEndPoint).Address.ToString();
                data = ASCIIEncoding.ASCII.GetString(rawdata, 0, (int)size);
                masterIp = ip;

                //received ping from control node
                if (data[0] == '+')
                {
                    string ping = "|+";
                    byte[] bytes = ASCIIEncoding.ASCII.GetBytes(ping);
                    udp.send(masterIp, bytes, (uint)bytes.Length);
                    Library.WriteErrorLog("Ping Received");
                    continue;
                }

                if (data.Substring(0, 9) == "|shutdown")
                {
                    Library.WriteErrorLog("Shutdown command initiated");
                    Process.Start("shutdown", "/s /f /t 0");
					continue;
                }

                if(data.Substring(0,8) == "|restart")
                {
                    Library.WriteErrorLog("Restart command initiated");
                    Process.Start("shutdown", "/r /f /t 0");
					continue;
                }

                if (data.Substring(0, 6) == "|sleep")
                {
                    Dispatcher.Invoke(new System.Action(() =>
                    {
                        Status("Sleep", ip);
                    }));                
                    Library.WriteErrorLog("Sleep command initiated");
                    string ping = "Sleep command received";
                    byte[] bytes = ASCIIEncoding.ASCII.GetBytes(ping);
                    udp.send(masterIp, bytes, (uint)bytes.Length);
                    SetSuspendState(true, true, false);                 
                    continue;
                }

                if (data.Substring(0, 6) == "|start")
                {
                    Dispatcher.Invoke(new System.Action(() =>
                    {
                        Status("VMS Client process start", ip);
                    }));                   
                    //kill the current running process first
                    Process[] process = Process.GetProcessesByName("VMS_Client");

                    foreach (Process pr in process)
                    {
                        pr.Kill();
                    }

                    Thread.Sleep(2000);

                    //ProcessStartInfo startInfo = new ProcessStartInfo(dir + "Start.bat");
                    //startInfo.Password = RunProcess.ConvertToSecureString("15jan1992");
                    //RunProcess.StartProcess(startInfo);
                    Process proc = new Process();
                    proc.StartInfo.FileName = dir + @"Start.bat";
                    proc.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(dir + @"Start.bat");
                    proc.Start();
                    //Process.Start(dir + @"VMS_Client.exe");
                    //ProcessRun.ProcessAsUser.Launch(dir + @"VMS_Client.exe");
                    Library.WriteErrorLog("Starting process...");
                    Thread.Sleep(2000);

                    string ping = string.Empty;
                    Process[] pname = Process.GetProcessesByName("VMS_Client");
                    if (pname.Length == 0)
                        ping = "Process failed to start";
                    else
                        ping = "Process started successfully";
                    byte[] bytes = ASCIIEncoding.ASCII.GetBytes(ping);
                    udp.send(masterIp, bytes, (uint)bytes.Length);
                    continue;
                }

                if (data.Substring(0, 5) == "|stop")
                {
                    Dispatcher.Invoke(new System.Action(() =>
                    {
                        Status("VMS Client process stop", ip);
                    }));
                    
                    Process[] process = Process.GetProcessesByName("VMS_Client");

                    foreach (Process pr in process)
                    {
                        pr.Kill();
                    }

                    Thread.Sleep(2000);

                    string ping = string.Empty;
                    Process[] pname = Process.GetProcessesByName("VMS_Client");
                    if (pname.Length == 0)
                        ping = "Process terminated successfully";
                    else
                        ping = "Process failed to terminate";
                    byte[] bytes = ASCIIEncoding.ASCII.GetBytes(ping);
                    udp.send(masterIp, bytes, (uint)bytes.Length);
                    continue;
                }

                if (data.Substring(0, 7) == "|update")
                {
                    Dispatcher.Invoke(new System.Action(() =>
                    {
                        Status("VMS Client software update", ip);
                    }));                    
                    //stop process
                    Process[] process = Process.GetProcessesByName("VMS_Client");
                    foreach (Process pr in process)
                    {
                        pr.Kill();
                    }
                    Thread.Sleep(2000);

                    string ping = string.Empty;
                    Process[] pname = Process.GetProcessesByName("VMS_Client");
                    if (pname.Length != 0)
                    {
                        ping = "Process failed to terminate. Update process terminated.";
                        byte[] bytess = ASCIIEncoding.ASCII.GetBytes(ping);
                        udp.send(masterIp, bytess, (uint)bytess.Length);
                        continue;
                    }

                    //run update. Only replace older file
                    Process proc = new Process();
                    proc.StartInfo.FileName = dir + @"UpdateClient.bat";
                    proc.StartInfo.WorkingDirectory = System.IO.Path.GetDirectoryName(dir + @"UpdateClient.bat");
                    proc.Start();

                    ////delete from local folder first
                    //string[] filePaths = Directory.GetFiles(dir);
                    //foreach (string filePath in filePaths)
                    //{
                    //    if (filePath.Contains("VMS_Client") || filePath.Contains("MySql"))
                    //    {
                    //        File.Delete(filePath);
                    //    }
                    //}

                    ////copy new file from server
                    //foreach (var file in Directory.GetFiles(@"\\192.168.1.82\ClickOnce\VMS Client\"))
                    //{
                    //    File.Copy(file, System.IO.Path.Combine(dir, System.IO.Path.GetFileName(file)));
                    //}

                    ping = "Update successful";
                    byte[] bytes = ASCIIEncoding.ASCII.GetBytes(ping);
                    udp.send(masterIp, bytes, (uint)bytes.Length);

                    Library.WriteErrorLog("Client software updated.");
                    //File.Copy(@"\\192.168.1.82\ClickOnce\VMS Client\VMS_Client.exe", @"D:\test.exe");
                    continue;
                }

                //the content of command should be look like this
                // |task:2000:0730
                // 0     6 8  1113
                if(data.Substring(0,5) == "|task")
                {
                    Dispatcher.Invoke(new System.Action(() =>
                    {
                        Status("Task Scheduler creation", ip);
                    }));            
                    try
                    {
                        double sleep_hour = double.Parse(data.Substring(6, 2));
                        double sleep_minutes = double.Parse(data.Substring(8, 2));
                        double wake_hour = double.Parse(data.Substring(11, 2));
                        double wake_minutes = double.Parse(data.Substring(13, 2));

                        //task for sleeping
                        using (TaskService ts = new TaskService())
                        {
                            // Remove previous task
                            try { ts.RootFolder.DeleteTask(@"Sleep"); }
                            catch (Exception) { }

                            // Create a new task definition and assign properties
                            TaskDefinition td = ts.NewTask();
                            td.RegistrationInfo.Description = "Sleep the station.";
                            td.Settings.WakeToRun = true;
                            td.Settings.RunOnlyIfNetworkAvailable = true;

                            // Create a trigger that will fire the task at this time every other day. Default 7.30am
                            DailyTrigger dt = new DailyTrigger();
                            dt.StartBoundary = DateTime.Today + TimeSpan.FromHours(sleep_hour) + TimeSpan.FromMinutes(sleep_minutes);
                            dt.DaysInterval = 1;
                            td.Triggers.Add(dt);

                            // Create an action that will launch Notepad whenever the trigger fires
                            td.Actions.Add(new ExecAction(dir + @"sleep.bat", null, dir));

                            // Register the task in the root folder
                            ts.RootFolder.RegisterTaskDefinition(@"Sleep", td);
                        }

                        //task for waking the computer
                        using (TaskService ts = new TaskService())
                        {
                            // Remove previous task
                            try { ts.RootFolder.DeleteTask(@"Wake up"); }
                            catch (Exception) { }

                            // Create a new task definition and assign properties
                            TaskDefinition td = ts.NewTask();
                            td.RegistrationInfo.Description = "Wake up the station.";
                            td.Settings.WakeToRun = true;
                            td.Settings.RunOnlyIfNetworkAvailable = true;

                            // Create a trigger that will fire the task at this time every other day. Default 7.30am
                            DailyTrigger dt = new DailyTrigger();
                            dt.StartBoundary = DateTime.Today + TimeSpan.FromHours(wake_hour) + TimeSpan.FromMinutes(wake_minutes);
                            dt.DaysInterval = 1;
                            td.Triggers.Add(dt);

                            // Create an action that will launch Notepad whenever the trigger fires
                            td.Actions.Add(new ExecAction(dir + @"monitoron.bat", null, dir));

                            // Register the task in the root folder
                            ts.RootFolder.RegisterTaskDefinition(@"Wake up", td);
                        }

                        //task for running the client
                        using (TaskService ts = new TaskService())
                        {
                            // Remove previous task
                            try { ts.RootFolder.DeleteTask(@"Run Client"); }
                            catch (Exception) { }

                            // Create a new task definition and assign properties
                            TaskDefinition td = ts.NewTask();
                            td.RegistrationInfo.Description = "Run VMS Client.";
                            td.Settings.WakeToRun = true;
                            td.Settings.RunOnlyIfNetworkAvailable = true;

                            // Create a trigger that will fire the task at this time every other day. Default 7.30am
                            DailyTrigger dt = new DailyTrigger();
                            dt.StartBoundary = DateTime.Today + TimeSpan.FromHours(wake_hour) + TimeSpan.FromMinutes(wake_minutes + 1);
                            dt.DaysInterval = 1;
                            td.Triggers.Add(dt);

                            // Create an action that will launch Notepad whenever the trigger fires
                            td.Actions.Add(new ExecAction(dir + @"VMS_Client.exe", null, dir));
                            //td.Actions.Add(new ExecAction(dir + @"nircmd.exe", "monitor on", dir));

                            // Register the task in the root folder
                            ts.RootFolder.RegisterTaskDefinition(@"Run Client", td);
                        }
                        string send = "Task created successfuly";
                        Library.WriteErrorLog(send);
                        byte[] bytes = ASCIIEncoding.ASCII.GetBytes(send);
                        udp.send(masterIp, bytes, (uint)bytes.Length);
                    }
                    catch(Exception)
                    {
                        string send = "Task failed to create";
                        Library.WriteErrorLog(send);
                        byte[] bytes = ASCIIEncoding.ASCII.GetBytes(send);
                        udp.send(masterIp, bytes, (uint)bytes.Length);
                    }
                    continue;
                }
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);

            //kill udp thread
            _running = false;
            byte[] quit = { QUIT_FLAG };
            udp.send("127.0.0.1", quit, 1);
            udpProcessingThread.Abort();
            Library.WriteErrorLog("UDP thread stopped");

            //kill timer thread
            timer1.Enabled = false;
            Library.WriteErrorLog("Timer service stopped");
        }
    }
}
